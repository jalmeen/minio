import io.minio.Result;
import io.minio.errors.*;
import io.minio.messages.Item;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class MinioFileTasks extends MinioBucketTasks {

    private String bucketToBeInspected;
    private String objectNameToInspect;
    Scanner input = new Scanner(System.in);

    public void listObjectsFromBucket() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException {
        System.out.println("Enter the bucket name in from which you want to retrieve files : ");
        bucketToBeInspected = input.nextLine();

        bucketExist = minioClient.bucketExists(bucketToBeInspected);
        if(bucketExist){
            Iterable<Result<Item>> allAvailableObjects = minioClient.listObjects(bucketToBeInspected);

            for (Result<Item> result : allAvailableObjects) {
                Item item = result.get();
                System.out.println(item.lastModified() + ", " + item.objectName());
            }
        }
        //TODO : Add condition if bucket is empty
        else {
            System.out.println("mybucket does not exist");
        }
    }

    public void retrieveObjectInfo() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException, InvalidArgumentException {
        System.out.println("Enter bucket name and object name to retrieve : ");
        bucketToBeInspected = input.nextLine();
        objectNameToInspect = input.nextLine();

        minioClient.statObject(bucketToBeInspected,objectNameToInspect);
        InputStream stream = minioClient.getObject(bucketToBeInspected, objectNameToInspect);

        // Read the input stream and print to the console till EOF.
        byte[] buf = new byte[16384];
        int bytesRead;
        while ((bytesRead = stream.read(buf,0, buf.length)) >= 0) {
            System.out.println(new String(buf, 0, bytesRead));
        }
        // Close the input stream.
        stream.close();
    }

    public void uploadObject() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException, InvalidArgumentException {
        System.out.println("To which bucket you want to add Object? ");
        bucketToBeInspected = input.nextLine();
        System.out.println("Enter object name : ");
        objectNameToInspect = input.nextLine();
        System.out.println("Enter filePath of object : ");
        String filePath = input.nextLine();

        bucketExist = minioClient.bucketExists(bucketToBeInspected);

        if(bucketExist){
            minioClient.putObject(bucketToBeInspected,objectNameToInspect,filePath);
            System.out.println("File uploaded");
        }
        else
            System.out.println("Bucket does not exist");
    }

    public void removeObject() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException {

        System.out.println("Enter bucket name from which object should be deleted ");
        bucketToBeInspected = input.nextLine();
        System.out.println("Enter object name to delete : ");
        objectNameToInspect = input.nextLine();

        minioClient.removeObject(bucketToBeInspected,objectNameToInspect );
        System.out.println("Successfully removed ");
    }
}
