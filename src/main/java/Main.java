import io.minio.errors.*;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class Main {

    public static void main(String[] args) throws InvalidPortException, InvalidEndpointException, IOException, XmlPullParserException, NoSuchAlgorithmException, RegionConflictException, InvalidKeyException, ErrorResponseException, NoResponseException, InvalidBucketNameException, InsufficientDataException, InternalException, InvalidArgumentException {

        MinioFileTasks fileOperations = new MinioFileTasks();

        fileOperations.makeConnection();

        fileOperations.createBucket();
        fileOperations.displayListOfBuckets();
        fileOperations.deleteBucket();

        fileOperations.listObjectsFromBucket();
        fileOperations.retrieveObjectInfo();
        fileOperations.uploadObject();
        fileOperations.removeObject();
    }
}
