import io.minio.errors.*;
import io.minio.messages.Bucket;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Scanner;

public class MinioBucketTasks extends MinioConnection{

    protected boolean bucketExist;
    private String bucketToBeCreated;
    private String bucketToBeDeleted;


    public void createBucket() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException, RegionConflictException {
        System.out.println("Enter name of bucket to be created : ");
        Scanner input = new Scanner(System.in);
        bucketToBeCreated = input.nextLine();

        bucketExist = minioClient.bucketExists(bucketToBeCreated);

        if(bucketExist){
            System.out.println("Bucket with similar name already exist");
        }

        else {
            minioClient.makeBucket(bucketToBeCreated);
            System.out.println("Bucket : " + bucketToBeCreated + " has been successfully created");
        }
    }

    public void displayListOfBuckets() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException {
        List<Bucket> bucketList = minioClient.listBuckets();
        for (Bucket bucket : bucketList) {
            System.out.println(bucket.creationDate() + ", " + bucket.name());
        }
    }

    public void deleteBucket() throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException, InternalException, NoResponseException, InvalidBucketNameException, XmlPullParserException, ErrorResponseException {
        System.out.println("Enter bucket name to delete : ");
        Scanner input = new Scanner(System.in);
        bucketToBeDeleted = input.nextLine();

        bucketExist = minioClient.bucketExists(bucketToBeDeleted);
        if (bucketExist) {
            minioClient.removeBucket(bucketToBeDeleted);
            System.out.println("Bucket is removed successfully");
        }
        else {
            System.out.println("Bucket does not exist");
        }
    }
}
